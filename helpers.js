const path = require('path');
const pkAPIPath = path.resolve('/Users/jon/repos/pk-api');

function renameColumn({ dbCol, modelCol, fullContents }) {
    const renameRegex = new RegExp(`( +)${dbCol}`, 'g');

    fullContents = fullContents.replace(renameRegex, `$1${modelCol}`);
    fullContents = fullContents.replace(`Pk = '${dbCol}'`, `Pk = '${modelCol}'`);

    if (!fullContents.includes(`        field: '${dbCol}`)) {
        const withField = `${modelCol}: {\n        field: '${dbCol}',`;

        fullContents = fullContents.replace(`${modelCol}: {`, withField);
    }

    return fullContents;
}

module.exports = {
    pkAPIPath,
    renameColumn,
};