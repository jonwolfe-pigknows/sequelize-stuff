const path = require('path');
const fs = require('fs');
const { promiseFiles } = require('node-dir');

const { pkAPIPath } = require('./helpers');

process.on('unhandledRejection', err => { throw err; });

const modelsDir = path.join(pkAPIPath, 'src/models');

(async function iife() {
    const files = await promiseFiles(modelsDir);

    for (let fileIndex = 0; fileIndex < files.length; fileIndex++) {
        const filePath = files[fileIndex];

        if (!filePath.endsWith('.js')) continue;

        const contents = fs.readFileSync(filePath, 'utf-8');

        if (!contents.includes('ModelCtor')) {
            const fields = [];
            const lines = contents.split('\n');

            const lastImport = lines.length - ((lines.slice().reverse()).findIndex(i => i.includes(' from ')));
            const initIndex = lines.findIndex(i => i.endsWith('.init({'));
            const endingIndex = lines.findIndex(i => i === '  }, {');

            if (initIndex === -1 || endingIndex === -1) {
                console.warn(`nothing for ${path.basename(filePath)}`);
                continue;
            }

            let modelName = lines[initIndex].trim();
            modelName = modelName.substring(0, modelName.indexOf('.'));

            const body = lines.slice(initIndex + 1, endingIndex - 1);

            for (let bodyIndex = 0; bodyIndex < body.length; bodyIndex++) {
                const line = body[bodyIndex];
                const regex = /^ {4}(\w+)(?:\:|\,)/gi;
                const matches = regex.exec(line);

                if (matches && matches[1]) fields.push(matches[1]);
            }

            const jsdoc = ['/**'];

            const attributesAlreadyDefinedIndex = lines.findIndex(i => i.includes(`@typedef ${modelName}Attributes`));
            if (attributesAlreadyDefinedIndex === -1) {
                jsdoc.push(` * @typedef ${modelName}Attributes`);

                for (let fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
                    const field = fields[fieldIndex];
                    jsdoc.push(` * @property {any} ${field}`);
                }

                jsdoc.push(' *');
            }

            const modelAlreadyDefinedIndex = lines.findIndex(i => i.startsWith('* @typedef') && i.endsWith(`} ${modelName}Model`));
            if (modelAlreadyDefinedIndex === -1) {
                jsdoc.push(` * @typedef {Model<${modelName}Attributes> & ${modelName}Attributes} ${modelName}Model`);
            }

            jsdoc.push(` * @typedef {import('sequelize').ModelCtor<${modelName}Model>} ModelCtor`);
            jsdoc.push(' */');

            const newContents = [
                ...lines.slice(0, lastImport + 1),
                ...jsdoc,
                '',
                ...lines.slice(lastImport + 1)
            ];

            fs.writeFileSync(filePath, newContents.join('\n'));
        }
    }
})();