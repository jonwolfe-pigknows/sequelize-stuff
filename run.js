const fs = require('fs');
const path = require('path');
const dir = require('node-dir');

const { pkAPIPath, renameColumn } = require('./helpers');

process.on('unhandledRejection', err => { throw err; });

const data = fs.readFileSync(path.resolve('./data.txt'), 'utf-8');

const files = data.split('--------------------------------------------------------------------------').filter(Boolean);

(async function iife() {
    const existingFiles = await dir.promiseFiles(path.join(pkAPIPath, 'src/models'));
    // console.log(existingFiles);
    const safeExistingFiles = existingFiles.map(i => i.substring(0, i.length - 3).replace(/[^a-zA-Z0-9]/gi, '').toLowerCase());
    // console.log(safeExistingFiles);

    files.forEach(file => {
        const lines = file.split('\n').filter(Boolean);
        const filePath = lines[0].substring(0, lines[0].length - 1);
        const fullPath = path.join(pkAPIPath, filePath);
        const safeFullPath = fullPath.substring(0, fullPath.length - 3).replace(/[^a-zA-Z0-9]/gi, '').toLowerCase();
        // console.log(safeFullPath);

        const matchingIndex = safeExistingFiles.findIndex(i => i === safeFullPath
            || `${i}s` === safeFullPath
            || `${i}es` === safeFullPath
            || i === `${safeFullPath}s`
            || i === `${safeFullPath}es`
        );
        const matchingPath = existingFiles[matchingIndex];
        // console.log(matchingPath);

        if (matchingPath) {
            const origName = path.basename(fullPath).replace('.ts', '');
            const canonicalName = path.basename(matchingPath).replace('.ts', '');

            // console.log(origName, fullPath);
            // console.log(canonicalName, matchingPath);

            if (origName !== canonicalName) {
                console.log(`replace ${origName} with ${canonicalName}`);
            }

            if (0 !== 1) return;

            let fullContents = fs.readFileSync(matchingPath, 'utf-8');

            for (let index = 1; index < lines.length; index += 1) {
                const line = lines[index];
                const prev = lines[index - 1];

                let dbCol = '';
                let modelCol = '';

                let hasField = line.indexOf('field: \'');
                if (hasField != -1) {
                    dbCol = line.substring(hasField + 8, line.lastIndexOf('\''));

                    let matches = /^(?:export const )?(\w+)(?: |:)/i.exec(prev);
                    modelCol = matches[1];
                }


                if (dbCol && modelCol && dbCol !== modelCol) {
                    fullContents = renameColumn({ dbCol, modelCol, fullContents, });
                } else if (dbCol && !modelCol) {
                    console.group(filePath);
                    console.warn('dbCol', dbCol);
                    console.warn('modelCol', modelCol);
                    console.groupEnd();
                }
            }

            fs.writeFileSync(matchingPath, fullContents);
        } else {
            console.error('Cant find match for: ', filePath);
        }
    });
})();
