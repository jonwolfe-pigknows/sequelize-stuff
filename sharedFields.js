const fs = require('fs');
const path = require('path');
const dir = require('node-dir');

const { pkAPIPath, renameColumn } = require('./helpers');

process.on('unhandledRejection', err => { throw err; });

const sharedMappings = {
    sowId: 'sowid',
    boarId: 'boarid',
    gLine: 'gline',
    groupId: 'groupid',
    numberOfPigs: 'npigs',
};

(async function iife() {
    const files = await dir.promiseFiles(path.join(pkAPIPath, 'src/models'));

    for (let fileIndex = 0; fileIndex < files.length; fileIndex++) {
        const file = files[fileIndex];
        let fullContents = fs.readFileSync(file, 'utf-8');

        Object.entries(sharedMappings).forEach(([modelCol, dbCol]) => {
            fullContents = renameColumn({ dbCol, modelCol, fullContents, });
        });

        fs.writeFileSync(file, fullContents);
    }
}());